package database.page;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PageInfo {

    private Integer pageNumber;

    private Integer numberOfPages;

    private Integer numberOfItemsInPage;

}
