package database.page;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PageResponse<T> {

    private T response;

    private PageInfo pageInfo;

}
