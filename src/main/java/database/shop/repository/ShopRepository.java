package database.shop.repository;

import database.HibernateUtil;
import database.shop.domain.Shop;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ShopRepository {

    private ShopRepository() {}

    public static Shop saveOrUpdate(Shop shop) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.saveOrUpdate(shop);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return shop;
    }

    public static void deleteById(Long id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.delete(findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Shop not found")));
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Optional<Shop> findById(Long id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return Optional.of(session.get(Shop.class, id));
        } catch (Exception e) {
            e.printStackTrace();
        }

        throw new RuntimeException("Failed to find shop with id: " + id);
    }

    public static List<Shop> findAll() {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.createQuery("SELECT a FROM Shop a", Shop.class).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }
}
