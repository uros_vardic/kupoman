package database.shop.service;

import database.coupon.repository.CouponRepository;
import database.shop.domain.Shop;
import database.shop.repository.ShopRepository;

import java.util.List;
import java.util.Optional;

public class ShopService {

    public Shop saveOrUpdate(Shop shop) {
        return ShopRepository.saveOrUpdate(shop);
    }

    public void deleteById(Long id) {
        CouponRepository.findAll().stream()
                .filter(coupon -> coupon.getShop().getId().equals(id))
                .forEach(coupon -> CouponRepository.deleteById(coupon.getId()));

        ShopRepository.deleteById(id);
    }

    public Optional<Shop> findById(Long id) {
        return ShopRepository.findById(id);
    }

    public List<Shop> findAll() {
        return ShopRepository.findAll();
    }

}
