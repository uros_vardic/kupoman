package database.shop.rest;

import database.shop.domain.Shop;
import database.shop.service.ShopService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/shop")
public class ShopController {

    private final ShopService service = new ShopService();

    public ShopController() {}

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveOrUpdate(Shop shop) {
        return Response.status(200).entity(service.saveOrUpdate(shop)).build();
    }

    @DELETE
    @Path("/delete/{id}")
    public Response deleteById(@PathParam("id") Long id) {
        service.deleteById(id);

        return Response.status(200).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") Long id) {
        return service.findById(id)
                .map(shop -> Response.status(200).entity(shop).build())
                .orElseGet(() -> Response.status(404).build());
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        return Response.status(200).entity(service.findAll()).build();
    }
}
