package database.user.domain;

public enum PrivilegeLevel {

    OPERATOR, ADMINISTRATOR

}
