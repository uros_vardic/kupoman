package database.user.service;

import database.user.domain.User;
import database.user.repository.UserRepository;

import java.util.List;
import java.util.Optional;

public class UserService {

    public User saveOrUpdate(User user) {
        return UserRepository.saveOrUpdate(user);
    }

    public void deleteById(Long id) {
        UserRepository.deleteById(id);
    }

    public Optional<User> findById(Long id) {
        return UserRepository.findById(id);
    }

    public Optional<User> login(String username, String password) {
        return UserRepository.findAll()
                .stream()
                .filter(user -> user.getUsername().equals(username) &&
                        user.getPassword().equals(password))
                .findFirst();
    }

    public List<User> findAll() {
        return UserRepository.findAll();
    }

}
