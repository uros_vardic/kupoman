package database.user.rest;

import database.user.domain.User;
import database.user.service.UserService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/user")
public class UserController {

    private final UserService service = new UserService();

    public UserController() {}

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveOrUpdate(User user) {
        return Response.status(200).entity(service.saveOrUpdate(user)).build();
    }

    @DELETE
    @Path("/delete/{id}")
    public Response deleteById(@PathParam("id") Long id) {
        service.deleteById(id);

        return Response.status(200).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") Long id) {
        return service.findById(id)
                .map(user -> Response.status(200).entity(user).build())
                .orElseGet(() -> Response.status(404).build());
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(LoginRequest loginRequest) {
        return service.login(loginRequest.getUsername(), loginRequest.getPassword())
                .map(user -> Response.status(200).entity(user).build())
                .orElseGet(() -> Response.status(404).build());
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        return Response.status(200).entity(service.findAll()).build();
    }

}
