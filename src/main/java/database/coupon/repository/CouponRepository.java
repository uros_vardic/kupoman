package database.coupon.repository;

import database.HibernateUtil;
import database.coupon.domain.Coupon;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CouponRepository {

    private CouponRepository() {}

    public static Coupon saveOrUpdate(Coupon coupon) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.saveOrUpdate(coupon);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return coupon;
    }

    public static void deleteById(Long id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            session.delete(findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Coupon not found")));
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Optional<Coupon> findById(Long id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return Optional.of(session.get(Coupon.class, id));
        } catch (Exception e) {
            e.printStackTrace();
        }

        throw new RuntimeException("Failed to find coupon with id: " + id);
    }

    public static List<Coupon> findAll() {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.createQuery("SELECT a FROM Coupon a", Coupon.class).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

}
