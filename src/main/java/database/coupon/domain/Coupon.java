package database.coupon.domain;

import database.shop.domain.Shop;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "coupon")
@Data
@NoArgsConstructor
public class Coupon {

    @Id
    @Column(name = "coupon_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    @Column(name = "version")
    private Integer version;

    @ManyToOne
    @JoinColumn(name = "shop", nullable = false)
    private Shop shop;

    @Column(name = "product", nullable = false)
    private String product;

    @Column(name = "discounted_price", nullable = false)
    private Float discountedPrice;

    @Column(name = "original_price", nullable = false)
    private Float originalPrice;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "valid_from", nullable = false)
    private Date validFrom;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "valid_to")
    private Date validTo;

    public String getValidFrom() {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm").format(validFrom);
    }

    public void setValidFrom(String validFrom) {
        try {
            this.validFrom = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(validFrom);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getValidTo() {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm").format(validTo);
    }

    public void setValidTo(String validTo) {
        try {
            this.validTo = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(validTo);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
