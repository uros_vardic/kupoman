package database.coupon.rest;

import database.coupon.domain.Coupon;
import database.coupon.service.CouponService;
import database.page.PageResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/coupon")
public class CouponController {

    private final CouponService service = new CouponService();

    public CouponController() {}

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveOrUpdate(Coupon coupon) {
        if (coupon.getShop() == null)
            return Response.status(500).build();

        return Response.status(200).entity(service.saveOrUpdate(coupon, coupon.getShop())).build();
    }

    @DELETE
    @Path("/delete/{id}")
    public Response deleteById(@PathParam("id") Long id) {
        service.deleteById(id);

        return Response.status(200).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") Long id) {
        return service.findById(id)
                .map(coupon -> Response.status(200).entity(coupon).build())
                .orElseGet(() -> Response.status(404).build());
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        return Response.status(200).entity(service.findAll()).build();
    }

    @GET
    @Path("/all/{pageNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public PageResponse<List<Coupon>> findAll(@PathParam("pageNumber") Integer pageNumber) {
        return service.findAll(pageNumber);
//        return Response.status(200).entity(service.findAll(pageNumber)).build();
    }

    @GET
    @Path("/allActive/{pageNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAllActive(@PathParam("pageNumber") Integer pageNumber) {
        return Response.status(200).entity(service.findAllActive(pageNumber)).build();
    }

}
