package database.coupon.service;

import database.coupon.domain.Coupon;
import database.coupon.repository.CouponRepository;
import database.page.PageInfo;
import database.page.PageResponse;
import database.shop.domain.Shop;
import database.shop.repository.ShopRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CouponService {

    public Coupon saveOrUpdate(Coupon coupon, Shop shop) {
        if (shopDoesntExist(shop))
            coupon.setShop(ShopRepository.saveOrUpdate(shop));

        return CouponRepository.saveOrUpdate(coupon);
    }

    private boolean shopDoesntExist(Shop shop) {
        return shop == null || shop.getId() == null ||
                !ShopRepository.findById(shop.getId()).isPresent();
    }

    public void deleteById(Long id) {
        CouponRepository.deleteById(id);
    }

    public Optional<Coupon> findById(Long id) {
        return CouponRepository.findById(id);
    }

    public List<Coupon> findAll() {
        return CouponRepository.findAll();
    }

    public PageResponse<List<Coupon>> findAllActive(Integer pageNumber) {
        List<Coupon> coupons = CouponRepository.findAll()
                .stream()
                .filter(this::couponActiveFilter)
                .collect(Collectors.toList());

        return createPageResponse(pageNumber, coupons);
    }

    private boolean couponActiveFilter(Coupon coupon) {
        Date now = new Date();

        if (getAsDate(coupon.getValidTo()) == null)
            return now.after(getAsDate(coupon.getValidFrom()));

        return now.after(getAsDate(coupon.getValidFrom()))
                && now.before(getAsDate(coupon.getValidTo()));
    }

    private Date getAsDate(String dateString) {
        try {
            return new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        throw new RuntimeException();
    }

    public PageResponse<List<Coupon>> findAll(Integer pageNumber) {
        return createPageResponse(pageNumber, CouponRepository.findAll());
    }

    private static Integer itemCounter;

    private PageResponse<List<Coupon>> createPageResponse(Integer pageNumber, List<Coupon> coupons) {
        PageInfo pageInfo = createPageInfo(pageNumber, coupons.size());

        PageResponse<List<Coupon>> pageResponse = new PageResponse<>();
        pageResponse.setPageInfo(pageInfo);

        itemCounter = (pageNumber - 1) * pageInfo.getNumberOfItemsInPage();

        List<Coupon> response = coupons.stream()
                .filter(coupon -> pageFilter(coupon, pageNumber, pageInfo.getNumberOfItemsInPage()))
                .collect(Collectors.toList());

        System.out.println(response);

        pageResponse.setResponse(response);

        return pageResponse;
    }

    private PageInfo createPageInfo(Integer pageNumber, Integer numberOfCoupons) {
        PageInfo pageInfo = new PageInfo();
        pageInfo.setNumberOfItemsInPage(20);
        pageInfo.setNumberOfPages(numberOfCoupons / 20 + 1);

        if (pageNumber <= 0 || pageNumber > pageInfo.getNumberOfPages())
            throw new IllegalArgumentException("Page number is too high");

        pageInfo.setPageNumber(pageNumber);

        return pageInfo;
    }

    private boolean pageFilter(Coupon coupon, Integer pageNumber, Integer itemsInPage) {
        return coupon.getId() >= (pageNumber - 1) * itemsInPage &&
                itemCounter++ < pageNumber * itemsInPage;
    }

}
